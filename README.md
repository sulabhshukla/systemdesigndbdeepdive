# System Design: DB Deep Dive

## Objective

Anyone in the engineering organization - including Software Architect, Engineering Managers or Software Engineers can expect to grapple with the questions this gitbook aspires to answer.
This can also be a great source for interview preparation if you want are targeting any of the above roles. Being able select and justify your choice for the data storage technologies is one of the backbones of the standard System Design Interview.


